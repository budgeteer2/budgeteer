module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: [
    'plugin:vue/essential',
    '@vue/airbnb',
    '@vue/typescript/recommended',
  ],
  parserOptions: {
    ecmaVersion: 2020,
  },
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'quotes': [0, 'double'],
    'no-underscore-dangle': [0, "allow"],
    'max-classes-per-file': ["error", 2],
    "prefer-destructuring": ["warn", {"object": false, "array": false}]
  },
};
