import Vue from 'vue';
import Vuex from 'vuex';
import budgets from './budgets';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    drawer: null,
    loading: false,
    snackbar: {
      show: false,
      message: "",
    },
  },
  mutations: {
    setDrawer(state, payload) {
      state.drawer = payload;
    },
    setLoading(state, payload) {
      state.loading = payload;
    },
    setShowSnackbar(state, payload) {
      state.snackbar = payload;
    },
  },
  actions: {
    setDrawer(context, value) {
      context.commit('setDrawer', value);
    },
    setLoading(context, value) {
      context.commit('setLoading', value);
    },
    setShowSnackbar(context, value) {
      context.commit('setShowSnackbar', value);
    },
  },
  modules: {
    budgets,
  },
});
