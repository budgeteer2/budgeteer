import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';
import Dashboard from '../views/Dashboard.vue';
import Budget from '../views/Budget.vue';
import Transactions from '../views/Transactions.vue';
import store from '../store';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Dashboard',
    component: Dashboard,
  },
  {
    path: '/budgets/:budget/:item',
    name: 'BudgetItem',
    component: Budget,
  },
  {
    path: '/transactions',
    name: 'Transactions',
    component: Transactions,
  },

  {
    path: '/404',
    component: () => import('../views/404.vue'),
  },
  {
    path: '*',
    component: () => import('../views/404.vue'),
  },
];

const router = new VueRouter({
  routes,
  mode: 'history',
});

router.beforeEach((to, from, next) => {
  store.dispatch('setLoading', true);

  next();
});

router.afterEach(() => {
  store.dispatch('setLoading', false);
});

export default router;
