/**
 * Split the budget into multiple BudgetItemDatas depending on the given date
 * Eg. date given is 13.12.2020, amount is 3500€, range is 'month'
 * ... Then we split 3500 into the days until end of month and create BudgetItemDatas
 */

/* eslint-disable */

import { gql, GQLClient } from "@/graphql";
import { GraphqlResponse } from "@/graphql/types";
import { differenceInCalendarDays, eachDayOfInterval, format, parseISO } from "date-fns";
import eventBus from "./eventBus";

function roundDown(val: number) {
  return Math.floor(val * 100) / 100;
}

interface FindManyBudgetItemDataResponse {
  findManyBudgetItemData: [
    {
      id: number;
      amount: number;
      spent: number;
      date: string;
    }
  ]
}

interface FindFirstBudgetItemResponse {
  findFirstBudgetItem: {
    id: number;
  }
}

export default async function splitBudgetData(start: Date, end: Date, amount: number, budgetItem: string) {

  const diff = differenceInCalendarDays(end, start);
  const eachDay = eachDayOfInterval({ start, end })
  const amountPerDay = amount / (diff + 1); // Since today is not factored in, we have to add one day on top.

  const data: GraphqlResponse<FindFirstBudgetItemResponse> = await GQLClient.send({
    query: gql`
      query($name: String) {
        findFirstBudgetItem(where: {
          name: {
            equals: $name
          }
        }) {
          id
        }
      }`,
    variables: {
      name: budgetItem,
    },
  });

  const budgetItemId = data?.data?.findFirstBudgetItem?.id;

  const itemData: GraphqlResponse<FindManyBudgetItemDataResponse> = await GQLClient.send({
    query: gql`
        query($id: Int, $start: DateTime, $end: DateTime) {
          findManyBudgetItemData(where: {
            budgetItemId: {
              equals: $id
            }
            date: {
              gte: $start
            }
            AND: {
              date: {
                lte: $end
              }
            }
          }) {
            id
            amount
            spent
            date
          }
        }
      `,
    variables: {
      id: budgetItemId,
      start,
      end,
    },
  });

  const prepareCreateData = GQLClient.prepare({
    query: gql`
    mutation($amount: Float!, $date: DateTime!, $id: Int) {
      createBudgetItemData(data: {
        amount: $amount,
        spent: 0,
        isLocked: false,
        date: $date,
        budgetItem: {
          connect: {
            id: $id
          }
        }
      }) {
        id
        amount
      }
    }
    `,
    variables: {
      amount: 0,
      date: '',
      id: 0,
    },
  });
  const prepareUpdateData = GQLClient.prepare({
    query: gql`
    mutation($id: Int, $amount: Float!) {
      updateBudgetItemData(where: {id: $id}, data: {
        amount: {
          set: $amount
        }
      }) {
        id
        date
        budgetItem {
          name
        }
      }
    }    
    `,
    variables: {
      id: 0,
      amount: 0,
    },
  });

  const budgetItemData = itemData?.data?.findManyBudgetItemData.map(x => ({
    id: x.id,
    amount: x.amount,
    spent: x.spent,
    date: format(parseISO(x.date), 'yyyy-MM-dd'),
  }));

  eventBus.$emit('budget-amount-updating');

  await Promise.all(eachDay.map(async (day: Date) => {
    const formattedDay = format(day, 'yyyy-MM-dd');
    const exists = budgetItemData?.filter(x => x.date === formattedDay);
    if (exists && exists.length > 0) {
      await prepareUpdateData.update({
        id: exists[0].id,
        amount: amountPerDay
      });
    } else {
      await prepareCreateData.update({
        amount: amountPerDay,
        date: formattedDay,
        id: budgetItemId,
      });
    }
  }));

  eventBus.$emit('budget-amount-updated');
}
