export default (event: Event) => {
  if (event) (event.currentTarget as HTMLInputElement).blur();
};
