import Vue from 'vue';
import VueCompositionAPI from '@vue/composition-api';
import VCurrencyField from 'v-currency-field';
import Chartkick from 'vue-chartkick';
import vueDebounce from 'vue-debounce';
import Chart from 'chart.js';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';
import App from './App.vue';

Vue.use(vueDebounce);
Vue.use(Chartkick.use(Chart));
Vue.use(VueCompositionAPI);
Vue.use(VCurrencyField, {
  locale: 'de-DE',
  currency: 'EUR',
  decimalLength: 2,
  autoDecimalMode: false,
  min: null,
  max: null,
  defaultValue: 0,
  valueAsInteger: false,
  allowNegative: true,
});

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
