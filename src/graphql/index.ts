import { GraphQLClient } from "./client";

export { gqlToString as gql } from "./client";

const GQLClient = new GraphQLClient("http://192.168.0.6:8081/graphql");

export { GQLClient };
