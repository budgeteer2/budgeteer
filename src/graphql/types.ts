export interface GraphQLVariables {
    [key: string]: any;
}

export interface GraphQLRequest {
    query: string;
    variables?: GraphQLVariables;
}

export declare interface GraphqlResponse<T> {
    data?: T;
    errors?: {
        message: string;
        locations?: [
            {
                line: number;
                column: number;
            }
        ];
        path?: string[];
        extensions?: any;
    };
}
