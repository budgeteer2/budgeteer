import { fetch } from "cross-fetch";
import { GraphQLRequest, GraphQLVariables, GraphqlResponse } from "./types";

export function gqlToString(
  literals: ReadonlyArray<string> | Readonly<string>,
): string {
  return literals.toString();
}

async function sendQuery<T>(
  url: string,
  options: GraphQLRequest,
): Promise<GraphqlResponse<T>> {
  const fetchOptionsForQuery: RequestInit = {
    method: "POST",
    mode: "cors",
    cache: "no-cache",
    credentials: "same-origin",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    redirect: "follow",
    referrerPolicy: "no-referrer",
  };

  const body: { query: string; variables?: GraphQLVariables } = {
    query: options.query,
  };

  if (options.variables) {
    body.variables = options.variables;
  }

  fetchOptionsForQuery.body = JSON.stringify(body);
  const response = await fetch(`${url}`, fetchOptionsForQuery);
  try {
    const json = await response.json();
    const result: GraphqlResponse<T> = {};
    if (json) result.data = json.data;
    if (json.errors) result.errors = json.errors;

    return result;
  } catch (e) {
    console.error(e);
    return e;
  }
}

export interface GraphQLReusableRequest<T> {
  update(variables?: GraphQLVariables): Promise<GraphqlResponse<T>>;
}

class ReusableRequest<T> implements GraphQLReusableRequest<T> {
  // eslint-disable-next-line no-useless-constructor
  constructor(
    private _client: GraphQLClient,
    private _options: GraphQLRequest,
  ) { }

  async update(variables?: GraphQLVariables): Promise<GraphqlResponse<T>> {
    if (variables) this._options.variables = { ...this._options.variables, ...variables };
    return this._client.send(this._options);
  }
}

export class GraphQLClient {
  // eslint-disable-next-line no-useless-constructor
  constructor(private _url: string) { }

  async send<T>(options: GraphQLRequest): Promise<GraphqlResponse<T>> {
    return sendQuery(this._url, options);
  }

  prepare<T>(options: GraphQLRequest): GraphQLReusableRequest<T> {
    return new ReusableRequest(this, { ...options });
  }
}
